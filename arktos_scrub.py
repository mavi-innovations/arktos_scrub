#!/usr/bin/env python
import numpy as np
from scipy.signal import butter, lfilter, freqz, periodogram
import csv
import os.path
import sys
import matplotlib.pyplot as plt

def get_conversiondata(dirpath, filepath):

    data_array = np.genfromtxt(open(os.path.join(dirpath, filepath), 'r'),
                            delimiter=',', skip_header=1,
                            usecols=(0,1,2,3,4,5,6,7,8,9,10),
                            dtype=float)
    return data_array

def get_testdata_day3(dirpath, filelist, file_index):

    data_array = np.genfromtxt(open(os.path.join(dirpath, filelist[file_index]), 'r'),
                            delimiter=',', skip_header=1, skip_footer=1,
                            usecols=(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14),
                            dtype=float)
    return data_array

def get_testdata_day1(dirpath, filelist, file_index):

    data_array = np.genfromtxt(open(os.path.join(dirpath, filelist[file_index]), 'r'),
                            delimiter=',', skip_header=1, skip_footer=1,
                            usecols=(0,1,2,3,4,5,6,7,8,9,10,11,12,13),#14),
                            dtype=float)
    return data_array

def convert_straingauge(val, offset, slope):
    return (val - offset) / (-slope)

def convert_dragloadcellfunction(val, offset, slope):
    return (val - offset) / (-slope)

def convert_dragloadcell(V, offset, factor):
    '''
    Convert Vout from the drag load cell to lbs force.

    Args::

        - V = Voltage
        - factor = conversion ractor (V/lbs force)

    Returns::

        - degrees deflected from 0
    '''

    noload= 1.90
    deltaV = V - noload + offset
    lsb = deltaV * (1/factor)
    return lsb

def convert_TEsensor(V, error_fix, factor):
    '''
    Convert Vout from DOG inclinometer to degrees OR accelerometer to
    g. This is determined by calculating the deltaV from the unloaded
    voltage output.

    Args::

        - V = Voltage
        - factor = conversion ractor (V/unit)

    Returns::

        - degrees deflected from 0
    '''

    V = V + error_fix
    horizontalV = 2.5
    deltaV = V - horizontalV
    val = deltaV * (1/factor)
    return val

def findgoodval(array, j, previous):
    '''
    Find the next good value (not dropped) in the column of the array
    and average that with the previous good value.

    Args::

        - array = column of the data array to scrub
        - j = row
        - previous = the previous good value found

    Returns::

        - The average of the two good values
    '''

    def search_upper(array, j):
        ''' Recursively search  for a good value.'''
        upper = array[j]
        if upper > 5:
            upper = search_upper(array, j+1)
        return upper

    upper = search_upper(array, j+1)

    val = (previous + upper) / 2

    return val

def butter_lowpass(cutoff, fs, order=5):

    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):

    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

def scrubconvertdata(array, convert_list):

    shape = array.shape
    smallest_val = 99999
    biggest_val = 0

    for col in range(0, shape[1]):

        if col < 11:
            convert = convert_list[:,col]
            prev_val = 0 #initialized here for memory over loops

        else:
            convert = 0
            prev_val = 0

        for row, val in enumerate(array[:,col]):

            if col < 11 and val > 5:
                #print 'before', array[row, col]
                array[row, col] = findgoodval(array[:, col], row, prev_val)
                #print 'after', array[row, col]

            prev_val = array[row, col]

            # if col == 3 and array[row, col] < smallest_val:
            #     smallest_val = array[row, col]
            #
            # if col == 3 and array[row, col] > biggest_val:
            #     biggest_val = array[row, col]

            if col == 0: # Front Strain Gauge
                array[row, col] = convert_straingauge(array[row, col], convert[0], convert[1])
                pass

            elif col == 1: # Rear Strain Gauge
                array[row, col] = convert_straingauge(array[row, col], convert[0], convert[1])
                pass

            elif col == 2: # Drag Load Cell
                array[row, col] = convert_dragloadcell(array[row, col], convert[0], convert[1])
                pass

            elif 3 <= col <= 6: # Dog Inclinometers
                array[row, col] = convert_TEsensor(array[row, col], convert[0], convert[1])
                if array[row, col] > 50:
                    array[row, col] = 50
                if array[row, col] < -50:
                    array[row, col] = -50
                pass

            elif 7 <= col <= 9: # 4030 Accelerometer. It experiences a -0.1V offset
                array[row, col] = convert_TEsensor(array[row, col], convert[0], convert[1])
                pass

            elif col == 10: # 4030 Accelerometer. It experiences a -0.1V offset
                array[row, col] = convert_TEsensor(array[row, col], convert[0], convert[1])
                pass

            elif col == 13: # GPS speed is in knots
                array[row, col] *= (1.852 * 2)
                pass
    #print 'biggest_val', biggest_val, 'smallest_val', smallest_val

    return array

def outputimages(array, legend_list, yaxis_label, title, std_dev):
    '''

    '''
    numsamples = len(array[0])
    step = 1./400 #1. because we must declare one of the values as a float
    xaxis = np.arange(0, (numsamples / 400.0), step) #400.0 because need a float

    fig = plt.figure(figsize=(20.0, 11.0))
    x1 = fig.add_subplot(111)

    for i in range(len(array)):
        x1.plot(xaxis, array[i])

    x1.set_ylabel(yaxis_label)
    x1.set_xlabel('seconds')
    x1.set_title(title)

    if len(array) == 2:
        avg_number1 = "{0:.2f}".format(round(array[1][0],2))
        std_number1 = "{0:.2f}".format(round(std_dev[0],2))
        avg_input_string1 = legend_list[1] % ('Avg')
        std_input_string1 = legend_list[1] % ('Std Dev')
        avg_words1 = "%s = %s" % (avg_input_string1, avg_number1)
        std_words1 = "%s = %s" % (std_input_string1, std_number1)
        plt.text(0.15, 0.98, avg_words1, ha='center', va='center', transform=x1.transAxes)
        plt.text(0.40, 0.98, std_words1, ha='center', va='center', transform=x1.transAxes)

        x1.legend([legend_list[0], avg_input_string1])

    if len(array) == 4:
        avg_number1 = "{0:.2f}".format(round(array[2][0],2))
        avg_number2 = "{0:.2f}".format(round(array[3][0],2))
        std_number1 = "{0:.2f}".format(round(std_dev[0],2))
        std_number2 = "{0:.2f}".format(round(std_dev[1],2))
        avg_input_string1 = legend_list[2] % ('Avg')
        avg_input_string2 = legend_list[3] % ('Avg')
        std_input_string1 = legend_list[2] % ('Std Dev')
        std_input_string2 = legend_list[3] % ('Std Dev')
        avg_words1 = "%s = %s" % (avg_input_string1, avg_number1)
        avg_words2 = "%s = %s" % (avg_input_string2, avg_number2)
        std_words1 = "%s = %s" % (std_input_string1, std_number1)
        std_words2 = "%s = %s" % (std_input_string2, std_number2)
        plt.text(0.15, 0.98, avg_words1, ha='center', va='center', transform=x1.transAxes)
        plt.text(0.15, 0.95, avg_words2, ha='center', va='center', transform=x1.transAxes)
        plt.text(0.40, 0.98, std_words1, ha='center', va='center', transform=x1.transAxes)
        plt.text(0.40, 0.95, std_words2, ha='center', va='center', transform=x1.transAxes)

        x1.legend([legend_list[0], legend_list[1], avg_input_string1, avg_input_string2])

    dirpath = '/home/otto/mavi/arktos_scrub/plots/'
    plt.savefig(os.path.join(dirpath, title), bbox_inches='tight')

    plt.close()

def get_day1_info(test_number):

    numfiles = 5
    file_index = test_number - 1
    file_name = 'USBArktosTest_'
    file_list = [(file_name + str(i) + '.csv') for i in range(1, (numfiles+1))]
    return file_list, file_index

def get_day2_info(test_number):

    file_index = test_number - 1
    file_list = ['Static Rolling Resistance (SRR) test 1', 'Level Crawl test 2', '5 Degree Incline Crawl test 3', 'Incremental Speed test 4']
    return file_list, file_index

def get_day3_info(test_number):

    numfiles = 14
    file_index = test_number - 1
    file_name = 'ArktosFieldTest_'
    file_list = [(file_name + str(i) + '.csv') for i in range(1, (numfiles+1))]
    return file_list, file_index

def calc_inertial_load(acc_list):
    craft_weight = 15457
    inertial_load_array = np.zeros((len(acc_list), 1), dtype=float)
    for i, val in enumerate(acc_list):
        inertial_load_array[i] = val * craft_weight
    return inertial_load_array

def vdv_data_out(data, filename):
    dirpath = '/home/otto/mavi/arktos_scrub/data/'
    time_array = np.arange(0.0025, (0.0025 * len(data) + 0.0025), 0.0025)
    data_out = np.column_stack((time_array, data))
    np.savetxt(os.path.join(dirpath, filename), data_out, delimiter=",")

def VDVbarplot(array, legend_list, yaxis_label, title):
    n = len(array[0])
    xticks = np.arange(n)
    bars = []
    barwidth = 0.35

    fig, ax = plt.subplots()
    bar1 = ax.bar(xticks - (barwidth/2), array[0], barwidth, color='r')
    bar2 = ax.bar(xticks + (barwidth/2), array[1], barwidth, color='b')


    ax.set_ylabel(yaxis_label)

    ax.set_xticks(xticks + barwidth / 2)
    ax.set_xlabel('Speed (km/h)')
    ax.set_xticklabels(('5 km/h', '10 km/h', '20 km/h'))
    ax.legend(legend_list[:])
    ax.set_title(title)

    plt.show()


def plotstuff(array, legend_list, yaxis_label, title):
    numsamples = len(array[0])
    step = 1./400 #1. because we must declare one of the values as a float
    xaxis = np.arange(0, (numsamples / 400.0), step) #400.0 because need a float

    fig = plt.figure()
    x1 = fig.add_subplot(111)

    for i in range(len(array)):
        x1.plot(xaxis, array[i])

    x1.set_ylabel(yaxis_label)
    x1.set_xlabel('seconds')
    x1.legend(legend_list[:])
    x1.set_title(title)

    plt.show()

def calc_vdv(sensors, day_number, speed):
    title_vdvy = 'Field Test %d - VDV%d_Yaxis_4030.csv' % (day_number, speed)
    title_vdvx = 'Field Test %d - VDV%d_Xaxis_4030.csv' % (day_number, speed)
    title_vdvz = 'Field Test %d - VDV%d_Zaxis_4030.csv' % (day_number, speed)
    title_vdv805M1 = 'Field Test %d - VDV%d_Zaxis_805M1.csv' % (day_number, speed)

    vdv_data_out(sensors[7], title_vdvx)
    vdv_data_out(sensors[8], title_vdvy)
    sensor9_offset = sensors[9] - 0.7
    vdv_data_out(sensor9_offset, title_vdvz)
    vdv_data_out(sensors[10], title_vdv805M1)
    print ('VDV Calculated')
    #####VDV Plotting#####
    VDV_day1 = [3.482, 7.921, 8.869]
    VDV_day3 = [1.767, 1.972, 1.971]
    vdv_legend = ['VDV Day 1',
                  'VDV Day 3']
    vdv_yaxis = 'Weighted r.m.s. Accelerations (m/s^2)'
    vdv_title = 'VDV Comparison between Field Test 1 and 3'

    # VDVbarplot([VDV_day1, VDV_day3], vdv_legend, vdv_yaxis, vdv_title)

def plot_window(sensors, psd, day_number, legend_list, inputtable_legend_list, yaxis_list, name_string, titles, engineeringunits_array):
    #plotstuff([sensors[0], sensors[1]], [legend_list[0], legend_list[1]], yaxis_list[0], titles[0])
    overview_yaxis = 'Drag (lbf)\nSpeed()'
    plotstuff([(sensors[2] / 1000), sensors[6], engineeringunits_array[240000:256000, 13]], [legend_list[2], legend_list[6], 'Speed'], yaxis_list[1], titles[1])
    #plotstuff([sensors[10]], [legend_list[10]], yaxis_list[3], title_805M1)
    #plotstuff([engineeringunits_array[0:-1, 13]], [legend_list[13]], 'Speed (km/h)', 'Craft Speed')
    #plotstuff([engineeringunits_array[Sl:Sh, 12]], [legend_list[12]], 'Longitude', 'Craft Speed')
    #plotstuff([engineeringunits_array[Sl:Sh, 11]], [legend_list[11]], 'Latitude', 'Craft Speed')

def dump_images(sensors, sensor_means, sensor_stds, psd, day_number, legend_list, inputtable_legend_list, yaxis_list, name_string, titles, window_Hz):

    outputimages([sensors[0], sensors[1], sensor_means[0], sensor_means[1]],
                 [legend_list[0], legend_list[1], inputtable_legend_list[0], inputtable_legend_list[1]],
                 yaxis_list[0], titles[0], [sensor_stds[0], sensor_stds[1]])
    # outputimages([sensors[2], sensor_means[2]],
    #              [legend_list[2], inputtable_legend_list[2]],
    #              yaxis_list[1], titles[1], [sensor_stds[2]])
    # outputimages([sensors[3], sensors[4], sensor_means[3], sensor_means[4]],
    #              [legend_list[3], legend_list[4], inputtable_legend_list[3], inputtable_legend_list[4]],
    #              yaxis_list[2], titles[2], [sensor_stds[3], sensor_stds[4]])
    # outputimages([sensors[5], sensors[6], sensor_means[5], sensor_means[6]],
    #              [legend_list[5], legend_list[6], inputtable_legend_list[5], inputtable_legend_list[6]],
    #              yaxis_list[2], titles[3], [sensor_stds[5], sensor_stds[6]])
    # outputimages([sensors[7], sensors[8], sensor_means[7], sensor_means[8]],
    #              [legend_list[7], legend_list[8], inputtable_legend_list[7], inputtable_legend_list[8]],
    #              yaxis_list[3], titles[4], [sensor_stds[7], sensor_stds[8]])
    # outputimages([sensors[9], sensor_means[9]],
    #              [legend_list[9], inputtable_legend_list[9]],
    #              yaxis_list[3], titles[5], [sensor_stds[9]])

    # if day_number == 3:
    #     outputimages([sensors[10], sensor_means[10]],
    #                  [legend_list[10], inputtable_legend_list[10]],
    #                  yaxis_list[3], titles[6], [sensor_stds[10]])

        # outputimages_psd([psd[0]], [legend_list[2]], yaxis_list[4], titles[7], window_Hz)
        # outputimages_psd([psd[1], psd[2]], [legend_list[5], legend_list[6]], yaxis_list[5], titles[8], window_Hz)
        # outputimages_psd([psd[3]], [legend_list[10]], yaxis_list[6], titles[9], window_Hz)
    print ('Images Output')

def outputimages_psd(array, legend_list, yaxis_label, title, window_Hz):

    figpsd = plt.figure(figsize=(20.0, 11.0))
    figpsd = plt.figure()
    xpsd = figpsd.add_subplot(111)
    for i in range(len(array)):
        freqs = array[i][0]
        ps = array[i][1]
        #indices = array[i][2]
        xpsd.semilogy(freqs, ps)
        #xpsd.plot(freqs[indices], ps[indices])

    xpsd.set_ylim(1e-8, 1e6)
    xpsd.set_ylabel(yaxis_label)
    xpsd.set_xlim(0, window_Hz)
    xpsd.set_xlabel('Frequency [Hz]')
    xpsd.set_title(title)

    xpsd.legend(legend_list[:])

    dirpath = '/home/otto/mavi/arktos_scrub/plots/'
    plt.savefig(os.path.join(dirpath, title), bbox_inches='tight')

    plt.close()

############################################################################################
def run(day_number, test_number, speed, name_string, plot_flag, output_flag, vdv_flag, sensors, sensor_means, sensor_stds, engineeringunits_array, psd, filter_Hz, window_Hz):
    print "Day number = %d \nSpeed = %d \nTest Number = %d" % (day_number, speed, test_number)

    legend_list = ['Front Arm Strain', 'Rear Arm Strain', 'Tow Bar Drag',
                   'Front Arm Incline', 'Rear Arm Incline', 'Craft Incline - X',
                   'Craft Incline - Y', 'Craft Acceleration - X', 'Craft Acceleration - Y',
                   'Craft Acceleration - Z', '805M1 Craft Acceleration - Z',
                   'Latitude', 'Longitude', 'Speed', 'Time',
                   'Inertial Load']

    inputtable_legend_list = ['Front Arm %s Strain', 'Rear Arm %s Strain', 'Tow Bar %s Drag',
                'Front Arm %s Incline', 'Rear Arm %s Incline', '%s Craft Incline - X',
                '%s Craft Incline - Y', '%s Craft Acceleration - X', '%s Craft Acceleration - Y',
                '%s Craft Acceleration - Z', '%s 805M1 Craft Acceleration - Z',
                'Latitude', 'Longitude', 'Speed', 'Time',
                'Inertial Load']

    yaxis_list = ['Torque (lb-in)', 'Force (lbf)', 'Incline (degrees from horizontal)', 'Acceleration (g-force)',
                  'Drag Load PSD (lbf^2 / Hz)', 'Craft Pitch and Roll PSD (g^2 / Hz)', 'Z Axis Accelerations PSD (g^2 / Hz)',
                  'Volts (V)']

    title_swstrain = 'Field Test %d - %s Swing Arm Strain' % (day_number, name_string)
    title_dragload = 'Field Test %d - %s Drag Load' % (day_number, name_string)
    title_swincline = 'Field Test %d - %s Swing Arm Incline' % (day_number, name_string)
    title_cincline = 'Field Test %d - %s Craft Incline' % (day_number, name_string)
    title_caccxy = 'Field Test %d - %s Craft Accelerations' % (day_number, name_string)
    title_caccz = 'Field Test %d - %s MEMS Z Accelerations' % (day_number, name_string)
    title_805M1 = 'Field Test %d - %s 805M1 Z Accelerations' % (day_number, name_string)
    title_dragloadPSD = 'Field Test %d - %s Drag Load PSD' % (day_number, name_string)
    title_cinclinePSD = 'Field Test %d - %s Craft Incline PSD' % (day_number, name_string)
    title_805M1PSD = 'Field Test %d - %s 805M1 Z Accelerations PSD' % (day_number, name_string)

    titles = [title_swstrain, title_dragload, title_swincline, title_cincline, title_caccxy,
              title_caccz, title_805M1, title_dragloadPSD, title_cinclinePSD, title_805M1PSD]

    if vdv_flag == 1:
        calc_vdv(sensors, day_number, speed)

    if output_flag == 1:
        dump_images(sensors, sensor_means, sensor_stds, psd, day_number, legend_list, inputtable_legend_list, yaxis_list, name_string, titles, window_Hz)

    if plot_flag == 1:
        plot_window(sensors, psd, day_number, legend_list, inputtable_legend_list, yaxis_list, name_string, titles, engineeringunits_array)

    scrub_flag = 0

#######################################################

def get_full_plot():
    day_number = 3
    test_number = 9
    speed = 5
    Sl = 0
    Sh = -1
    title = 'Incline Test - Drag Load, Craft Y-Axis Incline, and Speed'# % test_number
    return day_number, test_number, speed, Sl, Sh, title

def get_data(day_number, test_number):

    conversion_file = 'convrates.csv'
    if day_number == 1:

        dirpath = '/home/otto/mavi/arktos_scrub/data/Day1/'
        file_info = get_day1_info(test_number)
        conversion_values = get_conversiondata(dirpath, conversion_file)
        raw_array = get_testdata_day1(dirpath, file_info[0], file_info[1])

    elif day_number == 2:

        dirpath = '/home/otto/mavi/arktos_scrub/data/Day2/'
        file_info = get_day2_info(test_number)
        conversion_values = get_conversiondata(dirpath, conversion_file)
        raw_array = get_testdata_day1(dirpath, file_info[0], file_info[1])

    elif day_number == 3:

        dirpath = '/home/otto/mavi/arktos_scrub/data/Day3/'
        file_info = get_day3_info(test_number)
        conversion_values = get_conversiondata(dirpath, conversion_file)
        raw_array = get_testdata_day3(dirpath, file_info[0], file_info[1])

    return raw_array, conversion_values

def calc_psd(data, time_step):
    # ps = np.abs(np.fft.fft(data)**2)
    # freqs = np.fft.fftfreq(data.size, time_step)
    # sorted_indices = np.argsort(freqs)
    freqs, ps = periodogram(data, 400)


    return [freqs, ps]

def scrub_data(raw_array, conversion_values, speed, Sl, Sh):
    ###Scrubbing, converting, and calculating inertial load
    engineeringunits_array = scrubconvertdata(raw_array, conversion_values)
    print ('Data Scrubbed')

    ##################################

    # Filter requirements.
    order = 3
    fs = 400.0   # sample rate, Hz
    cutoff = speed * 3  # desired cutoff frequency of the filter, Hz
    b, a = butter_lowpass(cutoff, fs, order)

    #filter data
    sensors = []
    sensor_means = []
    sensor_stds = []
    psd = []
    for i in range (0, 11):
        sensors.append(butter_lowpass_filter(engineeringunits_array[Sl:Sh, i], cutoff, fs, order))
        mean = np.mean(sensors[i])
        sensor_means.append([mean] * len(sensors[i]))
        sensor_stds.append(np.std(sensors[i]))
        if i == 2 or i == 5 or i == 6 or i == 9 or i == 10:
           psd.append(calc_psd(sensors[i], 0.0025))
           print('Data PSD\'d')

    print ('Data Filtered')
    # #Inertial Load Calculation
    # drag_load_filtered = sensors[2]
    # inertial_load_array = calc_inertial_load(sensors[8])
    # for i, val in enumerate(drag_load_filtered):
    #     drag_load_filtered[i] = drag_load_filtered[i] -  inertial_load_array[i]


    return sensors, sensor_means, sensor_stds, engineeringunits_array, psd, cutoff#, drag_load_filtered, inertial_load_array



def main():

    plot_flag = 0
    output_flag = 1
    vdv_flag = 0

    ##day_number, test_number, speed, Sl, Sh, name_stringv
    day1_options = [[1, 1, 5, 10000, 13750, 'Avg 5k'],
                    [1, 1, 10, 199000, 201200, 'Avg 10k'],
                    [1, 1, 20, 636000, 637200, 'Avg 20k'],
                    [1, 5, 30, 434000, 435200, 'Avg 30k'],
                    [1, 5, 30, 423060, 441818, '0 to 30k'],
                    [1, 2, 5, 21500, 24000, '2 Inch Step 5k'],
                    [1, 2, 20, 400500, 403000, '2 Inch Step 20k'],
                    [1, 4, 5, 40000, 42000, '4 Inch Step 5k'],
                    [1, 4, 20, 696500, 698500, '4 Inch Step 20k']
                    # [1, 1, 5, 0, -1, 'Avg 5k']
                    ]

    day3_options = [[3, 5, 5, 48000, 52000, 'Avg 5k '],
                    [3, 5, 10, 140000, 142000, 'Avg 10k '],
                    [3, 5, 20, 196000, 197000, 'Avg 20k '],
                    [3, 9, 5, 48000, 51000, 'Impulse Response (x1) '],
                    [3, 9, 5, 48000, 60000, 'Impulse Response (x3) '],
                    [3, 9, 10, 68000, 112000, '15 Degree Incline Test ']
                    # [3, 6, 5, 234000, 246000, 'PSD for 10K to 20k with 15Hz LPF (max 75Hz)', 75],
                    # [3, 6, 20, 234000, 246000, 'PSD for 10K to 20k with 60Hz LPF (max 75Hz)', 75],
                    # [3, 6, 5, 234000, 246000, 'PSD for 10K to 20k with 15Hz LPF (max 20Hz)', 20],
                    # [3, 6, 20, 234000, 246000, 'PSD for 10K to 20k with 60Hz LPF (max 20Hz)', 20]
                    ]

    # for options in day1_options:
    #     print options
    #
    #     test_raw_array, conversion_values = get_data(options[0], options[1])
    #     sensors, sensor_means, sensor_stds, engineeringunits_array, psd, filter_Hz = scrub_data(test_raw_array, conversion_values,
    #                                                     options[2], options[3], options[4])
    #     print "Acquired new data"
    #
    #     run(options[0], options[1], options[2], options[5], plot_flag, output_flag, vdv_flag,
    #         sensors, sensor_means, sensor_stds, engineeringunits_array, psd, filter_Hz, options[6])

    for options in day3_options:
        print options

        test_raw_array, conversion_values = get_data(options[0], options[1])
        sensors, sensor_means, sensor_stds, engineeringunits_array, psd, filter_Hz = scrub_data(test_raw_array, conversion_values,
                                                        options[2], options[3], options[4])
        print "Acquired new data"

        run(options[0], options[1], options[2], options[5], plot_flag, output_flag, vdv_flag,
            sensors, sensor_means, sensor_stds, engineeringunits_array, psd, filter_Hz, 75)


if __name__ == '__main__':

    main()
    #main(sys.argv[1])
